package com.mortezaqn.myapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.mortezaqn.myapplication.Utils.getData
import com.mortezaqn.myapplication.Utils.getHeader
import com.mortezaqn.myapplication.Utils.getKey
import com.mortezaqn.myapplication.Utils.getRoot
import com.mortezaqn.myapplication.Utils.getValues
import com.mortezaqn.myapplication.databinding.ActivityMainBinding
import org.json.JSONObject

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val response = JSONObject(jsonStr)
        Log.v(TAG, "onCreate: $response")
        val data = jsonStr.getData()
        val result = getValues(data.getHeader().getKey(), data.getRoot())
        var strResult = "\n"
        var count = 0
        result.forEach { map ->
            strResult += "Size = ${result.size} \t"
            map.forEach { (key, value) ->
                strResult += "$count -- $key = $value \n"
                count++
            }
        }
        binding.tvMainResult.text = strResult
        Log.e(TAG, "result: ")
        Log.i(TAG, result.toString())
        Log.d(TAG, "strResult: $strResult")
        Log.e(TAG, "onCreate: done!")

    }
}