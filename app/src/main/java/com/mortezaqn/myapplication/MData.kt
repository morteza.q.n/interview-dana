package com.mortezaqn.myapplication

import org.json.JSONObject

data class MData(val `data`: Data) {
    data class Data(
        val header: List<Header>,
        val root: List<JSONObject>
    ) {
        data class Header(
            val name: String,
            val nameEng: String
        )
    }
}