package com.mortezaqn.myapplication

import org.json.JSONArray
import org.json.JSONObject

object Utils {

    fun String.getData(): JSONObject = JSONObject(this).getJSONObject("data")
    fun JSONObject.getHeader(): JSONArray = this.getJSONArray("header")
    fun JSONObject.getRoot(): JSONArray = this.getJSONArray("root")

    fun JSONArray.getKey(): Map<String, String> {
        val resultMapper: HashMap<String, String> = hashMapOf()
        for (i in 0 until this.length()) {
            val currentObj = this.getJSONObject(i)
            val currentName = currentObj.getString("name")
            val currentNameEng = currentObj.getString("nameEng")
            resultMapper[currentNameEng] = currentName
        }
        return resultMapper
    }

    fun getValues(header: Map<String, String>, root: JSONArray): List<Map<String, String>> {
        val result: ArrayList<Map<String, String>> = arrayListOf()
        for (i in 0 until root.length()) {
            val curRoot = root.getJSONObject(i)
            val rootKey = curRoot.keys()
            while (rootKey.hasNext()) {
                val curMapper: HashMap<String, String> = hashMapOf()
                val currentRootKey = rootKey.next()
                if (header[currentRootKey] != null)
                    curMapper[header[currentRootKey]!!] = curRoot.getString(currentRootKey)
                result.add(i,curMapper)
            }
        }
        return result
    }

}