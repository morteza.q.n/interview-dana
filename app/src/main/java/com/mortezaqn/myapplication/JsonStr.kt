package com.mortezaqn.myapplication

const val jsonStr = "{\n" +
        "  \"data\": {\n" +
        "    \"header\": [\n" +
        "      {\n" +
        "        \"name\": \"سريال\",\n" +
        "        \"nameEng\": \"Serial\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"بارکد\",\n" +
        "        \"nameEng\": \"Barcode\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"تاريخ لیبلینگ\",\n" +
        "        \"nameEng\": \"ProductDate\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"كد كالا\",\n" +
        "        \"nameEng\": \"GoodCode\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"کد فرعی\",\n" +
        "        \"nameEng\": \"SecGoodCode\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"شرح کالا\",\n" +
        "        \"nameEng\": \"GoodCodeName\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"ایران کد\",\n" +
        "        \"nameEng\": \"IranCode\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"GTIN\",\n" +
        "        \"nameEng\": \"GS1\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"شماره پالت\",\n" +
        "        \"nameEng\": \"Palet\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"نام پالت\",\n" +
        "        \"nameEng\": \"PaletSerial\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"شماره مرجع\",\n" +
        "        \"nameEng\": \"GoodIDRefer\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"شیفت\",\n" +
        "        \"nameEng\": \"ShiftName\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"شیفت پرسنل\",\n" +
        "        \"nameEng\": \"ShiftPersonnelName\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"سرپرست شیفت\",\n" +
        "        \"nameEng\": \"ShiftSupervisorName\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"زمان\",\n" +
        "        \"nameEng\": \"CreateDateTime\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"کاربر بسته بند\",\n" +
        "        \"nameEng\": \"PackingUser\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"وزن بسته پالت\",\n" +
        "        \"nameEng\": \" PaletWeight\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"برند\",\n" +
        "        \"nameEng\": \"Brand\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"گروه محصول\",\n" +
        "        \"nameEng\": \"ProductGroup\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"نام محصول\",\n" +
        "        \"nameEng\": \"ProductName\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"تاریخ انقضا\",\n" +
        "        \"nameEng\": \"ExpireDate\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"لات\",\n" +
        "        \"nameEng\": \"Lot\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"وضعیت تولید\",\n" +
        "        \"nameEng\": \"ProductionType\"\n" +
        "      },\n" +
        "      {\n" +
        "        \"name\": \"آقای حسنی\",\n" +
        "        \"nameEng\": \"PropTest\"\n" +
        "      }\n" +
        "    ],\n" +
        "    \"root\": [\n" +
        "      {\n" +
        "        \"Serial\": \"UABJ0499999999\",\n" +
        "        \"Barcode\": \"UABJ0499999999\",\n" +
        "        \"ProductDate\": \"1401/04/06\",\n" +
        "        \"GoodCode\": \"J0403\",\n" +
        "        \"SecGoodCode\": \"0101010004002\",\n" +
        "        \"GoodCodeName\": \"تونیک تقویت کننده موی آقایان وارداتی \",\n" +
        "        \"IranCode\": 0,\n" +
        "        \"GS1\": \"\",\n" +
        "        \"Palet\": \"CJ0403000001\",\n" +
        "        \"PaletSerial\": null,\n" +
        "        \"GoodIDRefer\": null,\n" +
        "        \"ShiftName\": \"روزکار\",\n" +
        "        \"ShiftPersonnelName\": \"-\",\n" +
        "        \"ShiftSupervisorName\": \"-\",\n" +
        "        \"CreateDateTime\": \"1401/04/06 13:47\",\n" +
        "        \"PackingUser\": \"شرکت  راي دانا سيستم\",\n" +
        "        \"PaletWeight\": null,\n" +
        "        \"Brand\": \"ژاک آندرل\",\n" +
        "        \"ProductGroup\": \"تونیک\",\n" +
        "        \"ProductName\": \"تونیک تقویت کننده موی آقایان وارداتی -ژاک آندرل فرانسه\",\n" +
        "        \"ExpireDate\": \"1401/04/13\",\n" +
        "        \"Lot\": \"111\",\n" +
        "        \"ProductionType\": \"بازیافتی\",\n" +
        "        \"PropTest\": \"تسسسسسسست\"\n" +
        "      }\n" +
        "    ]\n" +
        "  }\n" +
        "}\n"